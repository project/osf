<?php
  
/**
* Implements hook_uninstall()
*/ 
function osf_entities_uninstall() 
{ 
  include_once("osf_entities.module");
  
  osf_entities_remove_structure();
} 

/**
 * Implements hook_schema().
 */
function osf_entities_schema() {
  $schema = array();

  $schema['resource'] = array(
    'description' => 'The base table for resource.',
    'fields' => array(
      'rid' => array(
        'description' => 'The primary identifier for the resource.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The type (bundle) of this resource.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'label' => array(
        'description' => 'The title of the resource.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'uid' => array(
        'description' => 'ID of Drupal user creator.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the resource was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the resource was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'description' => array(
        'description' => 'resource description.',
        'type' => 'text',
      ),
    ),
    'primary key' => array('rid'),
  );

  $schema['resource_type'] = array(
    'description' => 'Stores information about all defined resource types.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique resource type ID.',
      ),
      'type' => array(
        'description' => 'The machine-readable name of this type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of this type.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ),
      'ignoreResource' => array(
        'description' => 'Ignore this synchronized resource type. Use this option to ignore a synchronized class. Ingored classes are not used anywhere in Drupal',
        'type' => 'int',
        'not null' => TRUE,
        'size' => 'tiny',
        'default' => 0,
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );
  
  $schema['osf_drid_registry'] = array(
    'description' => t('Registry of local Drupal Resource ID to OSF URIs.'),
    'fields' => array(
      'drid' => array(
        'description' => 'Local Drupal identifier for remote OSF URI',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'uri' => array(
        'description' => 'The Custom Dataset URI of this node.',
        'type' => 'varchar',
        'length' => 2048,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('drid'),
  );  
  
  // Create tables used for caching resource_type entities
  $cache_schema = drupal_get_schema_unprocessed('system', 'cache');
  
  $schema["cache_entity_resource_type"] = $cache_schema;
  $schema["cache_entity_resource_type"]['description'] = "Cache table used to store resource_type entity records.";
  

  return $schema;
}

/**
 * Implements hook_requirements().
 *
 * Ensure that the OSF-WS-PHP-API is around
 */
function osf_entities_requirements($phase) {
  
  $requirements = array();
  
  $t = get_t();
  
  $requirements['osf_entities_sync_structure'] = array(
    'title' => $t('OSF Entities Connector'),
    'value' => ' ',
  );
  
  $requirements['osf_entities_namespaces_csv_writable'] = array(
    'title' => $t('OSF Entities Connector'),
    'value' => ' ',
  );
  
  $classes = variable_get("osf_entities_classes", NULL);
  $properties = variable_get("osf_entities_properties", NULL);
  $propertiesByFields = variable_get("osf_entities_classes_by_bundles", NULL);
  $classesByBundles = variable_get("osf_entities_properties_by_field", NULL);
  
  if($classes === NULL || 
     $properties === NULL ||
     $propertiesByFields === NULL || 
     $classesByBundles === NULL)
  {
    $requirements['osf_entities_sync_structure']['value'] = $t('Synchronize Entities Bundles and Fields');
    $requirements['osf_entities_sync_structure']['description'] = $t('<a href="/admin/config/osf/entities" target="_blank">You must synchronize the entities bundles and fields for the Resource Type custum Entity Type</a> in order to use Entities in OSF for Drupal');
    $requirements['osf_entities_sync_structure']['severity'] = REQUIREMENT_WARNING;
  }
  else
  {    
    $requirements['osf_entities_sync_structure']['value'] = $t('Entities Bundles and Fields Synchronized');
    $requirements['osf_entities_sync_structure']['severity'] = REQUIREMENT_OK;
  }
  
  if(!@is_writable($_SERVER['DOCUMENT_ROOT'].base_path().libraries_get_path('OSF-WS-PHP-API')."/StructuredDynamics/osf/framework/namespaces.csv"))
  {
    $requirements['osf_entities_namespaces_csv_writable']['value'] = $t('Namespaces.csv not writable.');
    $requirements['osf_entities_namespaces_csv_writable']['description'] = $t('Make sure this file is writable by the web server: '.$_SERVER['DOCUMENT_ROOT'].base_path().libraries_get_path('OSF-WS-PHP-API')."/StructuredDynamics/osf/framework/namespaces.csv");
    $requirements['osf_entities_namespaces_csv_writable']['severity'] = REQUIREMENT_WARNING;
  }
  else
  {
    $requirements['osf_entities_namespaces_csv_writable']['value'] = $t('Namespaces.csv writable');
    $requirements['osf_entities_namespaces_csv_writable']['severity'] = REQUIREMENT_OK;
  }

  return $requirements;
}
