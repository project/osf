<p>
  The <strong>OSF for Drupal Search Settings</strong> panel provides the ability
  to copy key changes in these various search configuration settings. Once
  copied, the Drupal site in which these changes have been made will now
  operate using these new settings. Specific settings that may be copied
  and pasted are:
</p>
<p>
  <img src="<?php echo strip_tags($_GET['path']); ?>/doc/images/qb_search_settings.png" width="800" />
</p>
<p>
  Values within this panel may be copied and then pasted into Drupal to
  change the site's search behavior. Here is the screen for doing so within
  Drupal:
</p>
<p>
  <img src="<?php echo strip_tags($_GET['path']); ?>/doc/images/qb_search_settings_drupal.png" width="800" />
</p>
<div class="boxYellowSolid">
  <strong>Note</strong>: the <strong>OSF for Drupal Search Settings</strong> are
  entered from the Administrator's menu in Drupal. It is located at
  Administration &gt; Configuration &gt; OSF for Drupal Settings &gt; Search
  Settings.
</div>
<div class="boxYellowSolid">
  <strong>Note:</strong> you can enter either the full URI string for the
  property (what is available for pasting from the Query Builder) or you
  can use the machine name for the field rather than the URI.
</div>