<?php

use \StructuredDynamics\osf\php\api\ws\search\SearchQuery;
use \StructuredDynamics\osf\php\api\ws\sparql\SparqlQuery;
use \StructuredDynamics\osf\php\api\ws\ontology\read\OntologyReadQuery;
use \StructuredDynamics\osf\php\api\ws\ontology\read\GetLoadedOntologiesFunction;
use \StructuredDynamics\osf\framework\Namespaces;

/**
* Implements hook_menu()
* 
* @see http://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_menu/7
*/
function osf_views_menu()
{
  $items = array();

  $items['admin/config/osf/views'] = array(
    'title' => t('Views Connector Settings'), 
    'description' => t('Settings of the OSF Views plugin'),
    'page callback' => 'drupal_get_form', 
    'page arguments' => array( 'osf_views_settings' ),
    'access arguments' => array( 'administer osf views' ), 
    'type' => MENU_NORMAL_ITEM
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function osf_views_permission() {
  return array(
    'administer osf views' => array(
      'title' => 'Administer OSF Views',
    ),
  );
}

/**
* Defines the settings form.
* This function is called from the page callback of the menu system defined
* in the implementation of the hook_menu() (osf_views_hook()).
*/
function osf_views_settings()
{
  $form["osf_views_sync_structure_button"] = array(
    '#type' => 'button', 
    '#value' => t('Synchronize Ontologies Structures'),
  );  
         
  return system_settings_form($form);
}

/**
* Validates the form created by the osf_views_settings() function.
* 
* @param mixed $form Form to be validated
* @param mixed $form_state Form state to be validated
*/
function osf_views_settings_validate($form, &$form_state) 
{  
  if($form_state['clicked_button']['#parents']['0'] == "osf_views_sync_structure_button")
  {
    osf_views_cache_ontologies();
    
    if(count(drupal_get_messages('warning', FALSE)) == 0 &&
       count(drupal_get_messages('error', FALSE)) == 0)
    {
      drupal_set_message('OSF ontologies structures successfully synchronized');
    }
  }  
}

/**
* Implements hook_views_api() 
* 
* Register View API information. This is required for your module to have
* its include files loaded; for example, when implementing
* hook_views_default_views().
*
* @return
*   An array with the following possible keys:
*   - api:  (required) The version of the Views API the module implements.
*   - path: (optional) If includes are stored somewhere other than within
*       the root module directory, specify its path here.
*   - template path: (optional) A path where the module has stored it's views template files.
*        When you have specificed this key views automatically uses the template files for the views.
*        You can use the same naming conventions like for normal views template files.
*/
function osf_views_views_api() {
  return array(
    'api' => '3.0',
    'path' => drupal_get_path('module', 'osf_views') . '/views',
  );
}



/**
* Get the current OSF search query to send according 
* to the current filtering criterias defined for the view
* 
* @param mixed $view The view object currently being created by the user
* @return SearchQuery The SearchQuery object of the query
*/
function osf_views_get_search($view) {
  $network = variable_get("osf_views_views_target_osf_web_services_" . $view->name, "http://localhost/ws/");  
  $defaultEndpoint = osf_configure_get_endpoint_by_uri($network);      
    
  $search = new SearchQuery($defaultEndpoint->uri, $defaultEndpoint->app_id, $defaultEndpoint->api_key, osf_configure_get_current_user_uri());
  
  // Get all the fields and filters currently defined for this view
  $typeFilters = array();
  $attributeFilters = array();
  
  $filters = unserialize(urldecode(variable_get("osf_views_filters_" . $view->name)));

  $classes = NULL;
  
  if (is_array($filters) && !empty($filters)) {
    foreach ($filters as $filter => $value) {
      $coreFilter = $filter;
            
      if(strpos($filter, "_") !== FALSE) {
        $coreFilter = substr($filter, 0, strrpos($filter, "_"));
      }
      
      switch($coreFilter) {
        case "type":        
          // Here, depending if the filters come from a newly created filter
          // or not, we have to get them using two different methods.
          if (isset($value->typeFilters) && count($value->typeFilters) > 0) {
            // The current filter criterias are coming from a new filter 
            // that is currently being created
            $typeFilters = array_merge($typeFilters, $value->typeFilters);
          }
          else {
            array_push($typeFilters, $value->value);
          }
        break;
           
        case "datatypeproperty":
          $uri = substr($value->operator, 0, strpos($value->operator, "::"));
          $operator = substr($value->operator, strpos($value->operator, "::") + 2);
        
          if (!isset($attributeFilters[$uri])) {
            switch($operator) {
              case "contains":
                $attributeFilters[$uri] = array($value->value);
              break;
              
              case "starts with":
                $attributeFilters[$uri] = array($value->value."*");
              break;
            }         
          }
          else {
            switch($operator) {
              case "contains":
                array_push($attributeFilters[$uri], $value->value);
              break;
              
              case "starts with":
                array_push($attributeFilters[$uri], $value->value."*");
              break;
            }               
          }      
        break;  
        
        case "annotationproperty":
          $uri = substr($value->operator, 0, strpos($value->operator, "::"));
          $operator = substr($value->operator, strpos($value->operator, "::") + 2);
        
          if (!isset($attributeFilters[$uri])) {
            switch($operator) {
              case "contains":
                $attributeFilters[$uri] = array($value->value);
              break;
              
              case "starts with":
                $attributeFilters[$uri] = array($value->value."*");
              break;
            }         
          }
          else {
            switch($operator) {
              case "contains":
                array_push($attributeFilters[$uri], $value->value);
              break;
              
              case "starts with":
                array_push($attributeFilters[$uri], $value->value."*");
              break;
            }               
          }      
        break;        
  
        case "objectproperty":
          $uri = substr($value->operator, 0, strpos($value->operator, "::"));
          $operator = substr($value->operator, strpos($value->operator, "::") + 2);
        
          if (!isset($attributeFilters[$uri])) {
            switch($operator) {
              case "refers to":
                $attributeFilters[$uri] = array($value->value);
              break;
            }         
          }
          else {
            switch($operator) {
              case "refers to":
                array_push($attributeFilters[$uri], $value->value);
              break;
            }               
          }      
        break;
      }
    }
  }

  // Apply contextual arguments
  // TODO: Ensure that the args found in the URL match up in the correct order as the filters in the View. This is fine while there is only a single contextual filter.
  if (isset($view->argument) && !empty($view->argument)) {
    foreach ($view->argument as $arg_key => $arg_value) {
      switch ($arg_key) {
        case 'type':
          foreach (variable_get('osf_entities_classes_by_bundles', array()) as $key => $value) {
            if (in_array($key, $view->args)) {
              $typeFilters[] = $value;
            }
          }
          break;
        case 'keyword':
          // Apply the passed arguments as the keywords to search in prefLabel and description
          // @TODO Consider adding whatever URI for the article body here as well.
          if (!empty($arg_value->argument)) {
            $attributeFilters = array(
              "http://purl.org/ontology/iron#prefLabel" => array($arg_value->argument),
              "http://purl.org/ontology/iron#description" => array($arg_value->argument)
            );
          }
          break;
        
        default:
          # code...
          break;
      }
    }    
  }

  // Prepare the query to filter on types  
  if(count($typeFilters) > 0) {
    $search->typesFilters($typeFilters);
  }
  
  // Prepare the query to filter on the prefLabel of records  
  if(count($attributeFilters) > 0) {
    $search->attributesValuesFilters($attributeFilters);
  }

  // Return the SearchQuery object. That way, the code that calls osf_views_get_search()
  // will be able to continue defining the Search query for its own needs. (by 
  // example, disabling the aggregations, etc). 
  return($search);  
}

/**
 * Minimize queries by creating a single request for these settings.
 */
function osf_views_get_attributes_and_filters($view) {
  $search = &drupal_static(__FUNCTION__);
  if (!isset($search)) {
    // Get the list of all the properties that are used
    // to describe records of this OSF instance
    $search = osf_views_get_search($view);
    
    // Here we make sure that we don't ally any attribute, nor types, filters
    // we want to make sure that all the attributes will be returned for 
    // defining field.
    $search->attributesValuesFilters("all");
    $search->typesFilters("all");
    $search->send(new DrupalQuerierExtension());
  }
  return $search;
}

/**
* Cache the ontologies descriptions of each OSF defined
* for this module. 
*/
function osf_views_cache_ontologies()
{
  $networks = osf_configure_get_endpoints(NULL, FALSE, TRUE);
  
  // TODO: This should use the cache API, and not system variables 
  // Delete all the caches for the labels
  variable_del("osf_views_datatypeproperty_labels");
  variable_del("osf_views_objectproperty_labels");
  variable_del("osf_views_annotationproperty_labels");
  variable_del("osf_views_undefinedproperty_labels");
  variable_del("osf_views_class_labels");  

  $dataPropertiesPrefLabel = array();
  $objectPropertiesPrefLabel = array();
  $annotationPropertiesPrefLabel = array();
  $undefinedPropertiesPrefLabel = array();
  $classPropertiesPrefLabel = array();
  
  foreach($networks as $network)
  {
    // Get the list of all the ontologies currently loaded on this OSF instance.
    $defaultEndpoint = osf_configure_get_endpoint_by_uri($network->uri);

    $ontologyRead = new OntologyReadQuery($defaultEndpoint->uri, $defaultEndpoint->app_id, $defaultEndpoint->api_key, osf_configure_get_first_user_uri());
    
    $getLoadedOntologiesFunction = new GetLoadedOntologiesFunction();
    
    $getLoadedOntologiesFunction->modeUris();
    
    $ontologyRead->getLoadedOntologies($getLoadedOntologiesFunction);
    
    $ontologyRead->send(new DrupalQuerierExtension());

    if($ontologyRead->isSuccessful())
    {
      $resultset = $ontologyRead->getResultset()->getResultset();
      
      $ontologies = array();
      
      foreach($resultset as $dataset => $subjects)
      {
        foreach($subjects as $ontologyUri => $subject)
        {
          array_push($ontologies, $ontologyUri);
        }
      }     
    }
    else
    {
      drupal_set_message(t("Can't get the list of loaded ontologies: [@error] @errorMsg - @errorDescription", 
                   array("@error" => $ontologyRead->getStatus(),
                         "@errorMsg" => $ontologyRead->getStatusMessage(),
                         "@errorDescription" => $ontologyRead->getStatusMessageDescription())), 
                   "error", 
                   TRUE);
      return;
    }
    
    if(count($ontologies) == 0)
    {
      drupal_set_message(t("No ontologies loaded in the system. Please contact your system administrator to load the ontologies in OSF"), 
                         "error", 
                         TRUE);
      return;      
    }
    
    $labelProperties = Namespaces::getLabelProperties();
    
    // Get all the preferred labels for the data properties
    $labels = osf_views_get_ontologies_labels($network->uri, $labelProperties, $ontologies, Namespaces::$owl.'DatatypeProperty');
    $dataPropertiesPrefLabel[$network->uri] = $labels;
    
    // Get all the preferred labels for the object properties   
    $labels = osf_views_get_ontologies_labels($network->uri, $labelProperties, $ontologies, Namespaces::$owl.'ObjectProperty');
    $objectPropertiesPrefLabel[$network->uri] = $labels;
    
    // Get all the preferred labels for the annotations properties
    $labels = osf_views_get_ontologies_labels($network->uri, $labelProperties, $ontologies, Namespaces::$owl.'AnnotationProperty');
    $annotationPropertiesPrefLabel[$network->uri] = $labels;
    
    // Get all the preferred labels for the types (classes)
    $labels = osf_views_get_ontologies_labels($network->uri, $labelProperties, $ontologies, Namespaces::$owl.'Class');
    $classPropertiesPrefLabel[$network->uri] = $labels;
    
    $i = 0;   
  }
  
  // Now we check which properties are used to describe records in the defined
  // OSF instances. All the properties that are used and that are not
  // in one of the 3 lists created above will be part of this
  // undefined properties list.
  
  
  foreach($networks as $network)
  {     
    $defaultEndpoint = osf_configure_get_endpoint_by_uri($network->uri);
    
    $search = new SearchQuery($defaultEndpoint->uri, $defaultEndpoint->app_id, $defaultEndpoint->api_key, osf_configure_get_first_user_uri());
    
    $search->items(0);
    $search->includeAggregates();
    $search->send(new DrupalQuerierExtension());
    
    if(!$search->isSuccessful())
    {
      drupal_set_message(t("Can't get the list of classes and properties on this network @network : [@error] @errorMsg - @errorDescription", 
                         array("@error" => $search->getStatus(),
                               "@network" => $network->uri,
                               "@errorMsg" => $search->getStatusMessage(),
                               "@errorDescription" => $search->getStatusMessageDescription())), 
                         "error", 
                         TRUE);
    }
    else
    {        
      $resultset = $search->getResultset()->getResultset();
      
      $undefinedPropertiesPrefLabel[$network->uri] = array();
      
      foreach($resultset as $dataset => $subjects)
      {
        foreach($subjects as $uri => $subject)
        {
          if(array_search(Namespaces::$aggr."Aggregate", $subject["type"]) !== FALSE)
          {
            if($subject[Namespaces::$aggr."property"][0]["uri"] == Namespaces::$rdf."Property")
            {
              $propertyUri = $subject[Namespaces::$aggr."object"][0]["uri"];
              
              // Check if the property is a Datatype Property
              // Only the properties defined in the ontologies will be make available
              // to the user. If the node where the data come from hasn't defined a property
              // then it won't appear there neither, even if some records do use it
              if(!isset($datatypePropertiesPrefLabel[$network->uri][$propertyUri]) &&
                 !isset($objectPropertiesPrefLabel[$network->uri][$propertyUri]) &&
                 !isset($annotationPropertiesPrefLabel[$network->uri][$propertyUri]) &&
                 !isset($undefinedPropertiesPrefLabel[$network->uri][$propertyUri]))
              {
                $undefinedPropertiesPrefLabel[$network->uri][$propertyUri] = osf_views_generate_label_from_uri($propertyUri);
              }
            }
          }
        }
      }  
    }
  }    
  
  // Cache all labels retreived from all subscribed networks
  variable_set("osf_views_datatypeproperty_labels", $dataPropertiesPrefLabel);
  variable_set("osf_views_objectproperty_labels", $objectPropertiesPrefLabel);
  variable_set("osf_views_annotationproperty_labels", $annotationPropertiesPrefLabel);
  variable_set("osf_views_undefinedproperty_labels", $undefinedPropertiesPrefLabel);
  variable_set("osf_views_class_labels", $classPropertiesPrefLabel);   
}

function osf_views_get_ontologies_labels($network, $labelProperties, $ontologies, $type)
{
  // First, check how many results we should expect. If we get more than 500, 
  // we paginate the query
  
  $defaultEndpoint = osf_configure_get_endpoint_by_uri($network);
  
  $sparql = new SparqlQuery($defaultEndpoint->uri, $defaultEndpoint->app_id, $defaultEndpoint->api_key, osf_configure_get_first_user_uri());
  
  $sparql->mime("application/sparql-results+json");
  
  $query = "select count(distinct ?s) as ?nb \n";
  
  foreach($ontologies as $ontology)
  {
    $query .= "from named <$ontology> \n";
  }
   
  $query .= 'where
             {
               graph ?g
               {
                 ?s a <'.$type.'> .
                 ?s ?p ?label .
               
                 filter(?p in (
            ';
            
  foreach($labelProperties as $property)
  {
    $query .= "<$property>,";
  }            
  
  $query = trim($query, ",");
  
  $query .= ")) .}}";
  
  $sparql->query($query);
  
  $sparql->send(new DrupalQuerierExtension());
  
  $results = array();  
  
  if(!$sparql->isSuccessful())
  {
    drupal_set_message(t("Can't get ontologies labels: [@error] @errorMsg - @errorDescription", 
                       array("@error" => $sparql->getStatus(),
                             "@errorMsg" => $sparql->getStatusMessage(),
                             "@errorDescription" => $sparql->getStatusMessageDescription())), 
                       "error", 
                       TRUE);
  }
  else
  {  
    $json = json_decode($sparql->getResultset(), true);
    
    $nbResults = $json["results"]["bindings"][0]["nb"]["value"];
    $pageSize = 250;
    $nb = 0;
        
    $defaultEndpoint = osf_configure_get_endpoint_by_uri($network);        
        
    while($nb < $nbResults)
    {
      $sparql = new SparqlQuery($defaultEndpoint->uri, $defaultEndpoint->app_id, $defaultEndpoint->api_key, osf_configure_get_first_user_uri());
      
      $sparql->mime("application/sparql-results+json");
      
      $query = "select distinct ?s ?label \n";
      
      foreach($ontologies as $ontology)
      {
        $query .= "from named <$ontology> \n";
      }
       
      $query .= 'where
                 {
                   graph ?g
                   {
                     ?s a <'.$type.'> .
                     ?s ?p ?label .
                   
                     filter(?p in (
                ';
                
      foreach($labelProperties as $property)
      {
        $query .= "<$property>,";
      }            
      
      $query = trim($query, ",");
      
      $query .= ")) .}} offset $nb limit $pageSize";
      
      $sparql->query($query);
      
      $sparql->send(new DrupalQuerierExtension());
      
      if(!$sparql->isSuccessful())
      {
        drupal_set_message(t("Can't get the list of labels : [@error] @errorMsg - @errorDescription", 
                           array("@error" => $sparql->getStatus(),
                                 "@errorMsg" => $sparql->getStatusMessage(),
                                 "@errorDescription" => $sparql->getStatusMessageDescription())), 
                           "error", 
                           TRUE);
      }
      else
      {        
        $json = json_decode($sparql->getResultset(), true);
        
        foreach($json["results"]["bindings"] as $record)
        {
          $results[$record["s"]["value"]] = $record["label"]["value"];
        }    
        
        $nb += $pageSize;
      }
    }  
  }
  
  return($results);
}

function osf_views_views_ajax_data_alter(&$commands, $view){}

function osf_views_generate_label_from_uri($uri)
{
  // Get the ending of the URI
  $pos = strrpos($uri, "#");
  
  if($pos === FALSE)
  {
    $pos = strrpos($uri, "/");
  }
  
  $uri = substr($uri, ($pos + 1));
  
  // Now create a better label by splitting at the upper case letters
  $uri = preg_replace('/([a-z0-9])?([A-Z])/', '$1 $2', $uri);
  
  return($uri);
}

/**
 * Implementation of hook_cron().
 */
function osf_views_cron() {
  osf_views_cache_ontologies();
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Modify the views_ui_edit_form form to add a warning to the admin if 
 * osf_views query settings have not yet been configured.
 */
function osf_views_form_views_ui_edit_form_alter(&$form, &$form_state) {
  // Check for the Views target OSF variable for this view. If it is not set,
  // then this view has not been properly configured yet. We warn the admin before
  // they create the view that they should set this.
  $view = $form_state['view'];
  if ($view->base_table == 'osf_views_data') {
    // Add the warning message to the top of the Views admin UI
    $settings = variable_get('osf_views_views_target_osf_web_services_' . $view->name, FALSE);
    if (!$settings) {
      $attributes = array('attributes' => array('id' => 'views-page-query-warning', 'class' => array('views-ajax-link')));
      $link = l(('OSF settings'), 'admin/structure/views/nojs/display/' . $view->name . '/default/query', $attributes);
      $form['changed']['#markup'] .= '<br />* ' . t("You must select a OSF network before you can create your new View. !link", array('!link' => $link));
    }
  }
}

/**
 * Given a target URI and the name of the labels variable, returns the appropriate label.
 */
function osf_views_get_label($label_var_name, $target_uri) {
  $labels = variable_get($label_var_name, array());
  $label = "";
  
  if (!empty($labels)) {
    foreach ($labels as $item => $ls) {
      if (isset($ls[$target_uri])) {
        return $ls[$target_uri];
      }
    }
  }
  return $label;
}
