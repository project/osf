<?php
/**
 * @file 
 */


/**
 * Implements hook_schema().
 */
function osf_searchprofiles_schema() {
  $schema['osf_searchprofiles'] = array(
    'description' => t('Search profile.'),
    'export' => array(
      'key' => 'name',
      'key name' => 'Name',
      'primary key' => 'pid',
      'identifier' => 'sprofile',
      'default hook' => 'osf_searchprofiles_default_sprofile',
      'api' => array(
        'owner' => 'osf_searchprofiles',
        'api' => 'osf_searchprofiles',  // Base name for api include files.
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'name' => array(
        'type' => 'varchar',
        'length' => '255',
        'description' => 'Unique name for sprofiles. Used to identify them programmatically.',
      ),
      'pid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary ID field for the table. Used only for internal lookups.',
        'no export' => TRUE, // Do not export database-only keys.
      ),
      'description' => array(
        'description' => 'Description for this search profile.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE, 
        'default' => '',
      ),
      'settings' => array(
        'description' => 'Serialized storage of all search profile settings.',
        'type' => 'text',
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('pid'),
    'unique keys' => array(
      'name' => array('name'),
    ),
  );
  return $schema;
}
