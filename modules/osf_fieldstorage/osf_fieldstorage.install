<?php

  /**
  * Implement hook_disable()
  * 
  */
  function osf_fieldstorage_disable()
  {
    // Make sure that if OSF FieldStorage is disabled, that we revert the default
    // field storage system to the default one: field_sql_storage
    variable_set('field_storage_default', 'field_sql_storage');
  }
  
  $schema = array();
  
  /**
  * Implement hook_install()
  */
  function osf_fieldstorage_install()
  {
    $schema['osf_fieldstorage_pending_opts_fields'] = array(
      'description' => t('Keep track of the Content Type field changes for the OSF FieldStorage module'),
      'fields' => array(
        'id' => array(
          'type' => 'serial',
          'not null' => TRUE,
          'description' => 'Primary Key: Change Record ID.',
        ),          
        'bundle' => array(
          'description' => 'Bundle where the field instance did change',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ),
        'field' => array(
          'description' => 'Field instance where the changes took place',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ),
        'rdfmapping' => array(
          'description' => 'The new RDF Mapping for this field instance',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ),
        'prev_rdfmapping' => array(
          'description' => 'The previous RDF Mapping for this field instance, before rdfmapping get used',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ),
        'operation' => array(
          'description' => 'Operation that leaded to that change. Can be: new, changed or deleted',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ),
        'date' => array(
          'description' => 'Unix timestamp of when the change took place',
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
        'executed' => array(
          'description' => 'Specify if the change has been fully executed in the OSF instance',
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('id'),
    );  
    
    $schema['osf_fieldstorage_pending_opts_bundles'] = array(
      'description' => t('Keep track of the Content Type bundle mapping changes for the OSF FieldStorage module'),
      'fields' => array(
        'id' => array(
          'type' => 'serial',
          'not null' => TRUE,
          'description' => 'Primary Key: Change Record ID.',
        ),          
        'bundle' => array(
          'description' => 'Bundle where rdf mapping changed',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ),
        'rdfmapping' => array(
          'description' => 'The new RDF Mapping for this bundle instance',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ),
        'prev_rdfmapping' => array(
          'description' => 'The previous RDF Mapping for this bundle instance, before rdfmapping get used',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ),
        'operation' => array(
          'description' => 'Operation that leaded to that change. Can be: new, changed or deleted',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ),
        'date' => array(
          'description' => 'Unix timestamp of when the change took place',
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
        'executed' => array(
          'description' => 'Specify if the change has been fully executed in the OSF instance',
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('id'),
    );     
    
    $schema['osf_fieldstorage_revisions_binding'] = array(
      'description' => t('Keep track of the revisions timestamps in Drupal and the revisions URIs in OSF'),
      'fields' => array(
        'id' => array(
          'type' => 'serial',
          'not null' => TRUE,
          'description' => 'Primary Key',
        ),          
        'timestamp' => array(
          'description' => 'Timestamp of the revision in Drupal',
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
        'revuri' => array(
          'description' => 'Revision URI in OSF',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ),
      ),
      'primary key' => array('id'),
    );    
    
    db_create_table('osf_fieldstorage_pending_opts_fields', $schema['osf_fieldstorage_pending_opts_fields']);  
    db_create_table('osf_fieldstorage_pending_opts_bundles', $schema['osf_fieldstorage_pending_opts_bundles']);  
    db_create_table('osf_fieldstorage_revisions_binding', $schema['osf_fieldstorage_revisions_binding']);  
  }
  
  function osf_fieldstorage_uninstall()
  {
    db_drop_table('osf_fieldstorage_pending_opts_fields');
    db_drop_table('osf_fieldstorage_pending_opts_bundles');
    db_drop_table('osf_fieldstorage_revisions_binding');
  }
  
?>